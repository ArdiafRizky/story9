from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm,UserCreationForm
from django.contrib.auth.decorators import login_required

def get_loginPage(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request,user)
            request.session["user"] = request.POST["username"]
            return redirect("/success/")
        else:
            messages.error(request,"Incorrect account")

    form = AuthenticationForm()
    return render(request, "authApp/loginPage.html", {"form":form })

def get_registerPage(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request,user)
            request.session["user"] = request.POST["username"]
            return redirect("/success/")
        else:
            messages.error(request,"Please fill based on requirement")

    form = UserCreationForm()
    return render(request, "authApp/registerPage.html", {"form":form})

def get_logout(request):
    if request.method == "POST":
        logout(request)
    return redirect("authApp:loginPage")

@login_required(login_url="authApp:loginPage")
def get_successPage(request):
    username = request.session.get("user")
    return render(request, "authApp/successPage.html", {"username":username })