from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path
from .views import get_loginPage,get_registerPage,get_successPage,get_logout

app_name = "authApp"

urlpatterns = [
    path("", get_loginPage, name="loginPage"),
    path("register/", get_registerPage, name="registerPage"),
    path("success/", get_successPage, name="successPage"),
    path("logout/", get_logout, name="logout"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += staticfiles_urlpatterns()