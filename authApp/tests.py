from django.test import TestCase,Client
from django.urls import resolve
from . import views
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import unittest, time
from selenium.webdriver.common.keys import Keys
from datetime import date, time
from time import sleep
from django.test import LiveServerTestCase

# Create your tests here.
class TestAuthApp(TestCase):

    def test_access_authPage(self):
        response = Client().get("/")
        self.assertEqual(response.status_code, 200)

    def test_authPage_views(self):
        response = resolve("/")
        self.assertEqual(response.func, views.get_loginPage)

    def test_authPage_template(self):
        response = Client().get("/")
        self.assertTemplateUsed(response, "authApp/loginPage.html")

class TestFuncAuthApp(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(executable_path="./chromedriver", chrome_options=chrome_options)
        super(TestFuncAuthApp,self).setUp()
        
    def tearDown(self):
        self.browser.quit()
        super(TestFuncAuthApp,self).tearDown()

    def test_functional_login_before_register(self):
        driver = self.browser
        driver.get(self.live_server_url)

        username = driver.find_element_by_id("id_username")
        username.send_keys("saya")
        password = driver.find_element_by_id("id_password")
        password.send_keys("sayaganteng")
        submit = driver.find_element_by_id("submision")
        submit.click()
        sleep(2)
        self.assertNotIn("Hey",driver.page_source)
    
    def test_functional_access_register_page(self):
        driver = self.browser
        driver.get(self.live_server_url)

        redirect = driver.find_element_by_id("hyper")
        redirect.click()
        sleep(2)
        self.assertIn("Register",driver.page_source)
        
        # Continue here (register,login,etc)
        regName = driver.find_element_by_id("id_username")
        regName.send_keys("saya")
        pass1 = driver.find_element_by_id("id_password1")
        pass1.send_keys("sayaganteng")
        pass2 = driver.find_element_by_id("id_password2")
        pass2.send_keys("sayaganteng")
        sub = driver.find_element_by_id("submision-reg")
        sub.click()
        sleep(2)

        self.assertIn("Hey",driver.page_source)
        logout = driver.find_element_by_id("logout-btn")
        logout.click()
        sleep(2)
        self.assertIn("Login",driver.page_source)
        